package com.example.mlww.zaggy;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class GameActivity extends Activity {

    SharedPreferences pref;
    final Handler h = new Handler();
    private boolean gameOverCalled, ballRight;
    private Button playBtn, retryBtn;
    private TextView scoreOnBoard, highScoreOnBoard, score;
    private ImageView ball, gameOver, scoreBoard, logo,
            pillar1, pillar2, pillar3, pillar4, pillar5, pillar6, pillar7, pillar8, pillar9,
            pillar10, pillarTop1, pillarTop2, pillarTop3, pillarTop4, pillarTop5, pillarTop6;
    private int width, height, scoreValue, highScoreValue;
    private RelativeLayout layout;
    private SharedPreferences.Editor editor;
    private Rect ballRect = new Rect();
    private Rect pillar1Rect = new Rect();
    private Rect pillar2Rect = new Rect();
    private Rect pillar3Rect = new Rect();
    private Rect pillar4Rect = new Rect();
    private Rect pillar5Rect = new Rect();
    private Rect pillar6Rect = new Rect();
    private Rect pillar7Rect = new Rect();
    private Rect pillar8Rect = new Rect();
    private Rect pillar9Rect = new Rect();
    private Rect pillar10Rect = new Rect();
    private Rect pillarTop1Rect = new Rect();
    private Rect pillarTop2Rect = new Rect();
    private Rect pillarTop3Rect = new Rect();
    private Rect pillarTop4Rect = new Rect();
    private Rect pillarTop5Rect = new Rect();
    private Rect pillarTop6Rect = new Rect();

    private float density;


    private void getScreenResolution(Context context) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        DisplayMetrics metrics = new DisplayMetrics();
        display.getMetrics(metrics);
        width = metrics.widthPixels;
        height = metrics.heightPixels;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_game);

        density = getApplicationContext().getResources().getDisplayMetrics().density;

        pref = getApplicationContext().getSharedPreferences("com.example.mlww.zaggy_preferences.xml", 0);

        getScreenResolution(getApplicationContext());
        createUIViews();
        setProperties();

        playBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                playBtnListenerProperties();
                movement();
            }
        });
    }

    private void setProperties() {

        highScoreValue = pref.getInt("highscoresaved", 0);

        pillar1.setVisibility(View.INVISIBLE);
        pillar2.setVisibility(View.INVISIBLE);
        pillar3.setVisibility(View.INVISIBLE);
        pillar4.setVisibility(View.INVISIBLE);
        pillar5.setVisibility(View.INVISIBLE);
        pillar6.setVisibility(View.INVISIBLE);
        pillar7.setVisibility(View.INVISIBLE);
        pillar8.setVisibility(View.INVISIBLE);
        pillar9.setVisibility(View.INVISIBLE);
        pillar10.setVisibility(View.INVISIBLE);

        pillarTop1.setVisibility(View.INVISIBLE);
        pillarTop2.setVisibility(View.INVISIBLE);
        pillarTop3.setVisibility(View.INVISIBLE);
        pillarTop4.setVisibility(View.INVISIBLE);
        pillarTop5.setVisibility(View.INVISIBLE);
        pillarTop6.setVisibility(View.INVISIBLE);

        ball.setVisibility(View.INVISIBLE);
        score.setVisibility(View.INVISIBLE);
        scoreOnBoard.setVisibility(View.INVISIBLE);
        scoreBoard.setVisibility(View.INVISIBLE);
        gameOver.setVisibility(View.INVISIBLE);
        highScoreOnBoard.setVisibility(View.INVISIBLE);
        retryBtn.setVisibility(View.INVISIBLE);

        gameOverCalled = false;
        ballRight = true;
        scoreValue = 0;

    }

    void playBtnListenerProperties() {
        pillar1.setVisibility(View.VISIBLE);
        pillar2.setVisibility(View.VISIBLE);
        pillar3.setVisibility(View.VISIBLE);
        pillar4.setVisibility(View.VISIBLE);
        pillar5.setVisibility(View.VISIBLE);
        pillar6.setVisibility(View.VISIBLE);
        pillar7.setVisibility(View.VISIBLE);
        pillar8.setVisibility(View.VISIBLE);
        pillar9.setVisibility(View.VISIBLE);
        pillar10.setVisibility(View.VISIBLE);
        ball.setVisibility(View.VISIBLE);
        score.setVisibility(View.VISIBLE);

        pillarTop1.setVisibility(View.VISIBLE);
        pillarTop2.setVisibility(View.VISIBLE);
        pillarTop3.setVisibility(View.VISIBLE);
        pillarTop4.setVisibility(View.VISIBLE);
        pillarTop5.setVisibility(View.VISIBLE);
        pillarTop6.setVisibility(View.VISIBLE);

        logo.setVisibility(View.INVISIBLE);
        playBtn.setVisibility(View.INVISIBLE);

        int height1 = pillar1.getHeight();
        int width1 = pillar1.getWidth();

        pillar1.setX((width / 2) - 118.5f);
        pillar1.setY((height / 2) + 97.5f);
        ball.setX(width / 2);
        ball.setY(height / 2);
        pillar2.setX(pillar1.getX() + 118.5f);
        pillar2.setY(pillar1.getY() - 84.5f);
        pillar3.setX(pillar2.getX() + 118.5f);
        pillar3.setY(pillar2.getY() - 84.5f);

        pillarTop1.setX(pillar2.getX() + 10.5f);
        pillarTop1.setY(pillar2.getY() + 10.5f);
        pillarTop2.setX(pillar1.getX() + 10.5f);
        pillarTop2.setY(pillar1.getY() + 10.5f);

        pillar4.setX(pillarPlacementX(pillar3.getX()));
        pillar4.setY(pillarPlacementY(pillar3.getY()));
        pillar5.setX(pillarPlacementX(pillar4.getX()));
        pillar5.setY(pillarPlacementY(pillar4.getY()));
        pillar6.setX(pillarPlacementX(pillar5.getX()));
        pillar6.setY(pillarPlacementY(pillar5.getY()));
        pillar7.setX(pillarPlacementX(pillar6.getX()));
        pillar7.setY(pillarPlacementY(pillar6.getY()));
        pillar8.setX(pillarPlacementX(pillar7.getX()));
        pillar8.setY(pillarPlacementY(pillar7.getY()));
        pillar9.setX(pillarPlacementX(pillar8.getX()));
        pillar9.setY(pillarPlacementY(pillar8.getY()));
        pillar10.setX(pillarPlacementX(pillar9.getX()));
        pillar10.setY(pillarPlacementY(pillar9.getY()));

    }

    private void movement() {
        final int delay  = 45;

        h.postDelayed(new Runnable() {
            @SuppressLint("ClickableViewAccessibility")
            @Override
            public void run() {

                layout.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View view, MotionEvent motionEvent) {

                        if(motionEvent.getAction() == MotionEvent.ACTION_UP) {
                            if (!gameOverCalled) {
                                scoreValue++;
                                score.setText(String.valueOf(scoreValue));
                                ballRight = !ballRight;
                            }
                            return true;
                        }

                        return false;
                    }
                });

                if(ballRight) {
                    ball.setX(ball.getX() + 6.7f);
                    ball.setY(ball.getY() - 0.5f);
                } else {
                    ball.setX(ball.getX() - 6.7f);
                    ball.setY(ball.getY() - 0.5f);
                }

                pillar1.setY(pillar1.getY() + 5);
                pillar2.setY(pillar2.getY() + 5);
                pillar3.setY(pillar3.getY() + 5);
                pillar4.setY(pillar4.getY() + 5);
                pillar5.setY(pillar5.getY() + 5);
                pillar6.setY(pillar6.getY() + 5);
                pillar7.setY(pillar7.getY() + 5);
                pillar8.setY(pillar8.getY() + 5);
                pillar9.setY(pillar9.getY() + 5);
                pillar10.setY(pillar10.getY() + 5);
                ball.setY(ball.getY() + 0.5f);

                ball.getHitRect(ballRect);
                pillar1.getHitRect(pillar1Rect);
                pillar2.getHitRect(pillar2Rect);
                pillar3.getHitRect(pillar3Rect);
                pillar4.getHitRect(pillar4Rect);
                pillar5.getHitRect(pillar5Rect);
                pillar6.getHitRect(pillar6Rect);
                pillar7.getHitRect(pillar7Rect);
                pillar8.getHitRect(pillar8Rect);
                pillar9.getHitRect(pillar9Rect);
                pillar10.getHitRect(pillar10Rect);
                pillarTop1.getHitRect(pillarTop1Rect);
                pillarTop2.getHitRect(pillarTop2Rect);
                pillarTop3.getHitRect(pillarTop3Rect);
                pillarTop4.getHitRect(pillarTop4Rect);
                pillarTop5.getHitRect(pillarTop5Rect);
                pillarTop6.getHitRect(pillarTop6Rect);


                if(checkPillarPosition(pillar1.getY())) {
                    sendViewToBack(pillar1);
                    pillar1.setX(pillarPlacementX(pillar10.getX()));
                    pillar1.setY(pillarPlacementY(pillar10.getY()));
                } else if (checkPillarPosition(pillar2.getY())) {
                    sendViewToBack(pillar2);
                    pillar2.setX(pillarPlacementX(pillar1.getX()));
                    pillar2.setY(pillarPlacementY(pillar1.getY()));
                } else if (checkPillarPosition(pillar3.getY())) {
                    sendViewToBack(pillar3);
                    pillar3.setX(pillarPlacementX(pillar2.getX()));
                    pillar3.setY(pillarPlacementY(pillar2.getY()));
                } else if (checkPillarPosition(pillar4.getY())) {
                    sendViewToBack(pillar4);
                    pillar4.setX(pillarPlacementX(pillar3.getX()));
                    pillar4.setY(pillarPlacementY(pillar3.getY()));
                } else if (checkPillarPosition(pillar5.getY())) {
                    sendViewToBack(pillar5);
                    pillar5.setX(pillarPlacementX(pillar4.getX()));
                    pillar5.setY(pillarPlacementY(pillar4.getY()));
                } else if (checkPillarPosition(pillar6.getY())) {
                    sendViewToBack(pillar6);
                    pillar6.setX(pillarPlacementX(pillar5.getX()));
                    pillar6.setY(pillarPlacementY(pillar5.getY()));
                } else if (checkPillarPosition(pillar7.getY())) {
                    sendViewToBack(pillar7);
                    pillar7.setX(pillarPlacementX(pillar6.getX()));
                    pillar7.setY(pillarPlacementY(pillar6.getY()));
                } else if (checkPillarPosition(pillar8.getY())) {
                    sendViewToBack(pillar8);
                    pillar8.setX(pillarPlacementX(pillar7.getX()));
                    pillar8.setY(pillarPlacementY(pillar7.getY()));
                } else if (checkPillarPosition(pillar9.getY())) {
                    sendViewToBack(pillar9);
                    pillar9.setX(pillarPlacementX(pillar8.getX()));
                    pillar9.setY(pillarPlacementY(pillar8.getY()));
                } else if (checkPillarPosition(pillar10.getY())) {
                    sendViewToBack(pillar10);
                    pillar10.setX(pillarPlacementX(pillar9.getX()));
                    pillar10.setY(pillarPlacementY(pillar9.getY()));
                } else {
                    // do nothing
                }

                if(Rect.intersects(ballRect, pillarTop1Rect) || Rect.intersects(ballRect, pillarTop2Rect) || Rect.intersects(ballRect, pillarTop3Rect) || Rect.intersects(ballRect, pillarTop4Rect) ||
                                Rect.intersects(ballRect, pillarTop5Rect) || Rect.intersects(ballRect, pillarTop6Rect)) {

                } else {
                    gameOver();
                }

                if(Rect.intersects(ballRect, pillar1Rect)) {
                    pillarTop4.setVisibility(View.VISIBLE);
                    pillarTop5.setVisibility(View.VISIBLE);
                    pillarTop6.setVisibility(View.VISIBLE);
                    pillarTop1.setX(pillar2.getX() + 10.5f);
                    pillarTop2.setX(pillar1.getX() + 10.5f);
                    pillarTop3.setX(pillar10.getX() + 10.5f);
                    pillarTop4.setX(pillar9.getX() + 10.5f);
                    pillarTop5.setX(pillar8.getX() + 10.5f);
                    pillarTop6.setX(pillar7.getX() + 10.5f);
                    pillarTop1.setY(pillar2.getY() + 10.5f);
                    pillarTop2.setY(pillar1.getY() + 10.5f);
                    pillarTop3.setY(pillar10.getY() + 10.5f);
                    pillarTop4.setY(pillar9.getY() + 10.5f);
                    pillarTop5.setY(pillar8.getY() + 10.5f);
                    pillarTop6.setY(pillar7.getY() + 10.5f);

                } else if (Rect.intersects(ballRect, pillar2Rect)) {
                    pillarTop1.setX(pillar3.getX() + 10.5f);
                    pillarTop2.setX(pillar2.getX() + 10.5f);
                    pillarTop3.setX(pillar1.getX() + 10.5f);
                    pillarTop4.setX(pillar10.getX() + 10.5f);
                    pillarTop5.setX(pillar9.getX() + 10.5f);
                    pillarTop6.setX(pillar8.getX() + 10.5f);
                    pillarTop1.setY(pillar3.getY() + 10.5f);
                    pillarTop2.setY(pillar2.getY() + 10.5f);
                    pillarTop3.setY(pillar1.getY() + 10.5f);
                    pillarTop4.setY(pillar10.getY() + 10.5f);
                    pillarTop5.setY(pillar9.getY() + 10.5f);
                    pillarTop6.setY(pillar8.getY() + 10.5f);
                } else if (Rect.intersects(ballRect, pillar3Rect)) {
                    pillarTop4.setVisibility(View.INVISIBLE);
                    pillarTop5.setVisibility(View.INVISIBLE);
                    pillarTop6.setVisibility(View.INVISIBLE);
                    pillarTop1.setX(pillar4.getX() + 10.5f);
                    pillarTop2.setX(pillar3.getX() + 10.5f);
                    pillarTop3.setX(pillar2.getX() + 10.5f);
                    pillarTop1.setY(pillar4.getY() + 10.5f);
                    pillarTop2.setY(pillar3.getY() + 10.5f);
                    pillarTop3.setY(pillar2.getY() + 10.5f);
                } else if (Rect.intersects(ballRect, pillar4Rect)) {
                    pillarTop1.setX(pillar5.getX() + 10.5f);
                    pillarTop2.setX(pillar4.getX() + 10.5f);
                    pillarTop3.setX(pillar3.getX() + 10.5f);
                    pillarTop1.setY(pillar5.getY() + 10.5f);
                    pillarTop2.setY(pillar4.getY() + 10.5f);
                    pillarTop3.setY(pillar3.getY() + 10.5f);
                } else if (Rect.intersects(ballRect, pillar5Rect)) {
                    pillarTop1.setX(pillar6.getX() + 10.5f);
                    pillarTop2.setX(pillar5.getX() + 10.5f);
                    pillarTop3.setX(pillar4.getX() + 10.5f);
                    pillarTop1.setY(pillar6.getY() + 10.5f);
                    pillarTop2.setY(pillar5.getY() + 10.5f);
                    pillarTop3.setY(pillar4.getY() + 10.5f);
                } else if (Rect.intersects(ballRect, pillar6Rect)) {
                    pillarTop1.setX(pillar7.getX() + 10.5f);
                    pillarTop2.setX(pillar6.getX() + 10.5f);
                    pillarTop3.setX(pillar5.getX() + 10.5f);
                    pillarTop1.setY(pillar7.getY() + 10.5f);
                    pillarTop2.setY(pillar6.getY() + 10.5f);
                    pillarTop3.setY(pillar5.getY() + 10.5f);
                } else if (Rect.intersects(ballRect, pillar7Rect)) {
                    pillarTop1.setX(pillar8.getX() + 10.5f);
                    pillarTop2.setX(pillar7.getX() + 10.5f);
                    pillarTop3.setX(pillar6.getX() + 10.5f);
                    pillarTop1.setY(pillar8.getY() + 10.5f);
                    pillarTop2.setY(pillar7.getY() + 10.5f);
                    pillarTop3.setY(pillar6.getY() + 10.5f);
                } else if (Rect.intersects(ballRect, pillar8Rect)) {
                    pillarTop1.setX(pillar9.getX() + 10.5f);
                    pillarTop2.setX(pillar8.getX() + 10.5f);
                    pillarTop3.setX(pillar7.getX() + 10.5f);
                    pillarTop1.setY(pillar9.getY() + 10.5f);
                    pillarTop2.setY(pillar8.getY() + 10.5f);
                    pillarTop3.setY(pillar7.getY() + 10.5f);
                } else if (Rect.intersects(ballRect, pillar9Rect)) {
                    pillarTop1.setX(pillar10.getX() + 10.5f);
                    pillarTop2.setX(pillar9.getX() + 10.5f);
                    pillarTop3.setX(pillar8.getX() + 10.5f);
                    pillarTop1.setY(pillar10.getY() + 10.5f);
                    pillarTop2.setY(pillar9.getY() + 10.5f);
                    pillarTop3.setY(pillar8.getY() + 10.5f);
                } else if (Rect.intersects(ballRect, pillar10Rect)) {
                    pillarTop1.setX(pillar1.getX() + 10.5f);
                    pillarTop2.setX(pillar10.getX() + 10.5f);
                    pillarTop3.setX(pillar9.getX() + 10.5f);
                    pillarTop1.setY(pillar1.getY() + 10.5f);
                    pillarTop2.setY(pillar10.getY() + 10.5f);
                    pillarTop3.setY(pillar9.getY() + 10.5f);
                } else {

                }

                h.postDelayed(this, delay);
            }
        }, delay);


    }

    public void gameOver() {
        gameOverCalled = true;
        h.removeCallbacksAndMessages(null);

        sendViewToBack(pillar1);
        sendViewToBack(pillar2);
        sendViewToBack(pillar3);
        sendViewToBack(pillar4);
        sendViewToBack(pillar5);
        sendViewToBack(pillar6);
        sendViewToBack(pillar7);
        sendViewToBack(pillar8);
        sendViewToBack(pillar9);
        sendViewToBack(pillar10);

        pillar1.setVisibility(View.INVISIBLE);
        pillar2.setVisibility(View.INVISIBLE);
        pillar3.setVisibility(View.INVISIBLE);
        pillar4.setVisibility(View.INVISIBLE);
        pillar5.setVisibility(View.INVISIBLE);
        pillar6.setVisibility(View.INVISIBLE);
        pillar7.setVisibility(View.INVISIBLE);
        pillar8.setVisibility(View.INVISIBLE);
        pillar9.setVisibility(View.INVISIBLE);
        pillar10.setVisibility(View.INVISIBLE);

        pillarTop1.setVisibility(View.INVISIBLE);
        pillarTop2.setVisibility(View.INVISIBLE);
        pillarTop3.setVisibility(View.INVISIBLE);
        pillarTop4.setVisibility(View.INVISIBLE);
        pillarTop5.setVisibility(View.INVISIBLE);
        pillarTop6.setVisibility(View.INVISIBLE);

        if(scoreValue > highScoreValue) {
            highScoreValue = scoreValue;
            editor = pref.edit();
            editor.putInt("highscoresaved", highScoreValue);
            editor.apply();

            //maybe editor.commit();
        }

        scoreOnBoard.setText(String.valueOf(scoreValue));
        highScoreOnBoard.setText(String.valueOf(highScoreValue));
        ball.setVisibility(View.INVISIBLE);
        score.setVisibility(View.INVISIBLE);
        scoreOnBoard.setVisibility(View.VISIBLE);
        scoreBoard.setVisibility(View.VISIBLE);
        gameOver.setVisibility(View.VISIBLE);
        highScoreOnBoard.setVisibility(View.VISIBLE);
        retryBtn.setVisibility(View.VISIBLE);

        retryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setProperties();
                playBtnListenerProperties();
                movement();
            }
        });
    }

    public boolean checkPillarPosition(float y) {
        boolean low = false;

        if (y > height - height/3) {
            low = true;
        }
        return low;
    }

    public float pillarPlacementX(float x) {
        float pillarNewX;

        int random = (int) (Math.random() * 2 + 1);
        if (random == 1) {
            if (x > width - 120) {
                pillarNewX = x - 118.5f;
            } else {
                pillarNewX = x + 118.5f;
            }
        } else {
            if (x < 120) {
                pillarNewX = x + 118.5f;
            } else {
                pillarNewX = x - 118.5f;
            }
        }
        return pillarNewX;
    }

    public float pillarPlacementY(float y) {
        y = y - 84.5f;
        return y;
    }

    public static void sendViewToBack(final View child) {
        final ViewGroup parent = (ViewGroup) child.getParent();
        if(parent != null) {
            parent.removeView(child);
            parent.addView(child, 6);
        }
    }

    void createUIViews() {
        layout = findViewById(R.id.layout);
        playBtn = findViewById(R.id.playBtn);
        retryBtn = findViewById(R.id.retryBtn);
        scoreOnBoard = findViewById(R.id.scoreOnBoard);
        highScoreOnBoard = findViewById(R.id.highScoreOnBoard);
        score = findViewById(R.id.score);
        ball = findViewById(R.id.ball);
        gameOver = findViewById(R.id.gameOver);
        scoreBoard = findViewById(R.id.scoreBoard);
        logo = findViewById(R.id.logo);
        pillar1 = findViewById(R.id.pillar1);
        pillar2 = findViewById(R.id.pillar2);
        pillar3 = findViewById(R.id.pillar3);
        pillar4 = findViewById(R.id.pillar4);
        pillar5 = findViewById(R.id.pillar5);
        pillar6 = findViewById(R.id.pillar6);
        pillar7 = findViewById(R.id.pillar7);
        pillar8 = findViewById(R.id.pillar8);
        pillar9 = findViewById(R.id.pillar9);
        pillar10 = findViewById(R.id.pillar10);
        pillarTop1 = findViewById(R.id.pillarTop1);
        pillarTop2 = findViewById(R.id.pillarTop2);
        pillarTop3 = findViewById(R.id.pillarTop3);
        pillarTop4 = findViewById(R.id.pillarTop4);
        pillarTop5 = findViewById(R.id.pillarTop5);
        pillarTop6 = findViewById(R.id.pillarTop6);

    }
}
